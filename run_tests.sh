#!/usr/bin/env bash

#Test Cnf + Logs
exe=./build/satsol

for filename in ./dimacs/cnfs/*.cnf; do
    res=$(time $exe $filename)
    sat=$(echo $res | cut -d" " -f2)
    test=$(head -n 1 "$filename.log" | cut -d" " -f2)
    if [ "$test" == "$sat" ]
    then
        echo "$res OK"
    else
        echo "$res KO"
        exit 1
    fi
done

exit 0
