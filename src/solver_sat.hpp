//
// Created by sam on 24/10/18.
//

#ifndef SOLVER_SAT_CPP_SOLVER_SAT_HPP
#define SOLVER_SAT_CPP_SOLVER_SAT_HPP

#include <optional>
#include <vector>
#include "Sat.hpp"

using Literal=long;

/**
 * Solves the Sat problem
 * @param problem_sat the Sat varibale containing all the clauses
 * @return the vector of an interpretation is SAT else {}
 */
std::optional <Formula> solver_sat(const Sat &problem_sat);

#endif //SOLVER_SAT_CPP_SOLVER_SAT_HPP
