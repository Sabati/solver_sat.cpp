//
// Created by sam on 19/11/18.
//

#ifndef SATSOL_BRANCHING_HEURISTICS_H
#define SATSOL_BRANCHING_HEURISTICS_H

#include <queue>
#include <list>

#include "Sat.hpp"
std::priority_queue<std::pair<int,int>> most_occ_variables(const Sat&sat);
long most_occ_var(const Sat& sat,const std::list<Formula>& clauses);

#endif //SATSOL_BRANCHING_HEURISTICS_H
