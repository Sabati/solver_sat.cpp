//
// Created by sam on 19/11/18.
//

#include "branching_heuristics.h"

using namespace std;

std::priority_queue<pair<int,int>> most_occ_variables(const Sat &sat)
{
    vector<int> occurences(sat.number_of_variables, 0);
    for (auto &clause: sat.clauses)
    {
        for (int j = 0; j < sat.number_of_variables; ++j)
        {
            if (clause.lit[j] == 1)
                occurences[j] += 1;
            if (clause.neg_lit[j] == 1)
                occurences[j] += 1;
        }
    }

    priority_queue<pair<int, int>> vars; // pair<nombre d'occurences,index de la varible>
    for (size_t k = 0; k < occurences.size(); ++k)
    {
        vars.push(make_pair(occurences[k], k + 1));
    }

    return vars;
}

long most_occ_var(const Sat &sat,const list<Formula>& clauses)
{
    vector<int> occurences(sat.number_of_variables, 0);
    for (auto &clause: clauses)
    {
        for (int j = 0; j < sat.number_of_variables; ++j)
        {
            if (clause.lit[j] == 1)
                occurences[j] += 1;
            if (clause.neg_lit[j] == 1)
                occurences[j] += 1;
        }
    }
    return distance(occurences.cbegin(),max_element(occurences.cbegin(),occurences.cend()))+1;
}
