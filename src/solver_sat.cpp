//
// Created by sam on 24/10/18.
//

#include <queue>
#include <set>
#include <unordered_set>
#include "solver_sat.hpp"
#include "branching_heuristics.h"


using namespace std;


/**
 * Return true if there is an empty clause in the list
 * @param clauses a list of clauses
 * @return true id there is an empty clause in the list
 */
bool has_empty_clause(const list<Formula> &clauses)
{
    for (const auto &c : clauses)
    {
        if (c.is_empty())
            return true;
    }
    return false;
}

void simplify_unary_backtracking(list<Formula> &clauses, queue<Literal> &to_simplify, const Literal variable)
{
    auto clause_it = clauses.begin();
    while (clause_it != clauses.end())
    {
        if ((*clause_it)[variable])
        {
            clause_it = clauses.erase(clause_it);
        } else
        {
            if ((*clause_it)[-variable])
            {
                clause_it->flip(-variable);
                if (clause_it->is_unit())
                {
                    to_simplify.push(clause_it->get_unit());
                }
                ++clause_it;
            } else
            {
                ++clause_it;
            }
        }
    }
}

/**
 * Simplify the list of clauses, with the variables in the queue. For each simplification if in the clauses there is only
 * one variable it adds the variable to the queue. It there is no more unary clauses in the list.
 * @param clauses the list of clauses
 * @param to_simplify the queue filled with the variables to simplify.
 */
list<Formula> simplify_unary(list<Formula> clauses, queue<Literal> &to_simplify, Formula &model)
{
    while (!to_simplify.empty())
    {
        const auto variable = to_simplify.front();
        simplify_unary_backtracking(clauses, to_simplify, variable);
        model.flip(variable);
        to_simplify.pop();
    }
    return clauses;
}

/**
 * Simplify the list of clauses from all the unary clauses.
 * @param clauses the list of clauses
 */
list<Formula> simplify_unary(const list<Formula> &clauses, Formula &model)
{
    queue<Literal> to_simplify;
    for (const auto &c:clauses)
    {
        if (c.is_unit())
        {
            to_simplify.push(c.get_unit());
        }
    }
    return simplify_unary(clauses, to_simplify, model);
}

/**
 * Simplify the list of clauses from the variable in parameter.
 * @param clauses the list of clauses
 * @param variable the variable to simplify the list
 * @return the list of clauses simplified
 */
list<Formula> simplify(list<Formula> clauses, const Literal variable)
{
    auto clause_it = clauses.begin();
    while (clause_it != clauses.end())
    {
        if ((*clause_it)[variable])
        {
            clause_it = clauses.erase(clause_it);
        } else
        {
            if ((*clause_it)[-variable])
            {
                clause_it->flip(-variable);
                ++clause_it;
            } else
            {
                ++clause_it;
            }
        }
    }
    return clauses;
}

/**
 * Resolve the sat problem with a basic BackTracking with and unary simplification.
 * @param clauses the list of clauses
 * @param literal the literal seted
 * @return optional empty if it's UNSAT and a vector<int> with the solutions.
 */
optional<Formula>
backtracking(const Sat &sat, const list<Formula> &clauses, const Literal lit, Formula model)
{
    if (clauses.empty())
        return {model.flip(lit)};
    if (has_empty_clause(clauses))
        return {};

    auto next_clauses = simplify_unary(simplify(clauses, lit), model);
    auto next_variable = most_occ_var(sat, next_clauses);

    auto r = backtracking(sat, next_clauses, next_variable, model); // Positive branch
    if (r.has_value())
    {
        r->flip(lit);
        return {r};
    } else
    {
        next_clauses = simplify_unary(simplify(clauses, -lit), model);
        return backtracking(sat, next_clauses, -next_variable, model); // Negative Branch
    }
}

optional<Formula> solver_sat(const Sat &problem_sat)
{
    const list<Formula> clauses(problem_sat.clauses.cbegin(), problem_sat.clauses.cend());
    const auto lit = most_occ_var(problem_sat, clauses);
    auto model = Formula(problem_sat.number_of_variables);
    auto r = backtracking(problem_sat, clauses, lit, model);
    if (r.has_value())
    {
        return r;
    } else
    {
        return backtracking(problem_sat, clauses, -lit, model);
    }
}
